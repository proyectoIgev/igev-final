var numAnhos = 26;
var numProv = 9;
var numVehiculos = 6;
var lado = 15;
var max = 1000;

var colores = {
    0: {color: 'cyan', tipo:'barra'},
    1: {color:'red', tipo:'barra'},
    2: {color:'yellow', tipo:'barra'},
    3: {color: 'blue', tipo:'barra'},
    4: {color:'brown', tipo:'barra'},
    5: {color:'pink', tipo:'barra'}
};


var provinciasCyL = {
    "0": [-70,0,145],
    "1": [120,0,-90],
    "2": [-120,0,-120],
    "3": [-15,0,-40],
    "4": [-160,0,110],
    "5": [30,0,100],
    "6": [255,0,-10],
    "7": [-30,0,10],
    "8": [-185,0,30]
    };