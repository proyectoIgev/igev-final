var datos;
var barras;
var circular;

function read_data(){
    datos = (function () {
        var csv = null;
        $.ajax({
            url: "datos.csv",
            async: false,
            success: function (csvd) {
                csv = $.csv.toArrays(csvd);
            }, dataType: "text",
            complete: function () {
            }
        });
        return csv;
    })();
}


function init3dmap(){
    read_data()
    var vehiculos = Array(numVehiculos).fill(0);
    var anho = document.getElementById("anioMapa").value;
    var datos1 = Array(numProv).fill(null).map(()=>Array(numVehiculos).fill(0))
    console.log(anho);


    datos[1][1] = 0;
    console.log(datos1);

    for(var i = 1; i<= numVehiculos;i++){
        if(document.getElementById("checkbox"+i).checked) vehiculos[i-1]=1;
    }

    console.log(datos[1][(anho+ 5*numAnhos)*vehiculos[5]]);
    for( i = 1; i< numProv+1; i++){
        for( n = 1; n< numVehiculos+1; n++){
            datos1[i-1][n-1] = Number(vehiculos[n-1]) && datos[i][(Number(anho)+Number((n-1))*Number(numAnhos))];
        }
    }
    console.log(datos1);
    max = Math.max(max, datos[1]);

    TR3.pintar_barras(provinciasCyL, datos1, lado, colores, max);
}



function mostrar_barras(){
    read_data()
    if (barras){
        barras.destroy();
    }
    var datos1;
    var provincia = document.getElementById("provinciaBarras").value;
    var vehiculo = document.getElementById("vehiculoBarras").value;
    if (vehiculo == 7){
        datos1 = Array(numAnhos).fill(0);
        for (let i = 1; i< numAnhos+1 ; i++){   
            for (let n = 0; n< numVehiculos ; n++){      
                datos1[i-1] = Number(datos1[i-1]) + Number(datos[provincia][n*numAnhos+i]);
            }
        }
    }else{
        datos1 = datos[provincia].slice((vehiculo-1)*numAnhos+1, vehiculo*numAnhos+1);
    }
    
    var etiqueta;
    if(vehiculo == 1) etiqueta = "Numero de turismos por año"
    else if(vehiculo == 2)  etiqueta = "Numero de motocicletas por año"
    else if(vehiculo == 3)  etiqueta = "Numero de camiones y furgonetas pro año"
    else if(vehiculo == 4)  etiqueta = "Numero de autobuses por año"
    else if(vehiculo == 5)  etiqueta = "Numero de tractores industriales por año"
    else if(vehiculo == 6)  etiqueta = "Numero de otros vehiculos por año"
    else etiqueta = "Numero total de vehiculos por año"
    
    

    labelsAnio = ["1995", "1996", "1997", "1998", "1999", "2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020"]
    barras = new Chart(document.getElementById("bar-chart"), {
        type: 'bar',
        data: {
            labels: labelsAnio,
            datasets: [
            {
                barPercentage: 0.5,
                barThickness: 6,
                maxBarThickness: 8,
                minBarLength: 1,
                label: etiqueta,
                backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
                data: datos1
            }
            ]
        },
        options: {
            responsive: false,
            legend: { display: false },
            title: {
            display: true,
            text: 'Numero de vehiculos de un tipo en un año'
            },
            scales: {
            x: {
                grid: {
                    offset: true
                }
            }
        }
        }
    });
}

function mostrar_circular(){
    read_data()
    if (circular){
        circular.destroy();
    }

    var datos1;
    var provincia = document.getElementById("provinciaCircular").value;
    var anio = document.getElementById("anioCircular").value;

    datos1 = Array(numVehiculos).fill(0);
    for (let n = 0; n< numVehiculos ; n++){      
        datos1[n] = datos[provincia][n*numAnhos+Number(anio)];
    }

    circular = new Chart(document.getElementById("pie-chart"), {
        type: 'pie',
        data: {
        labels: ["Turismos", "Motocicletas", "Camiones y furgonetas", "Autobuses", "Tractores industriales","Otros"],
        datasets: [{
            label: "Numero de vehiculos",
            backgroundColor: ["#00ffff", "#ff0000","#ffff00","#0000ff","#A52A2A","#ffc0cb"],
            data: datos1
        }]
        },
        options: {
            responsive: false,
        title: {
            display: true,
            text: 'Porcenjate de vehiculos de cada tipo'
        }
        }
    });
}

